//
//  WeatherViewController.m
//  Weatherfy
//
//  Created by Eduardo Toledo on 12/13/17.
//  Copyright © 2017 SoSafe. All rights reserved.
//

#import "WeatherViewController.h"
#import "Weatherfy-Swift.h"
@import MapKit;

@interface WeatherViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *averageLabel;
@property (weak, nonatomic) IBOutlet UILabel *varianceLabel;

@property double variance;
@property double average;

@end

@implementation WeatherViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)onActionButtonPressed:(UIButton *)sender
{
    
    _average = [DataSource meanWithData:[DataSource data] in:[self.textField text]];
    _variance = [DataSource varianceWithData:[DataSource data] in: [self.textField text]];
    [self.textField resignFirstResponder];
    [self updateLabels];
    [self moveMapToCoordinates:CLLocationCoordinate2DMake(0, 0)];
}

- (void)updateLabels
{
    self.averageLabel.text = [NSString stringWithFormat:@"%.1lf", _average];
    self.varianceLabel.text = [NSString stringWithFormat:@"%.1lf", _variance];

}

- (void)moveMapToCoordinates:(CLLocationCoordinate2D)coordinates
{
    [self.mapView setCenterCoordinate:coordinates animated:YES];
}

@end
